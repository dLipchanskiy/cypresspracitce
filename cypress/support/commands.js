// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
require('cypress-downloadfile/lib/downloadFileCommand');


const Login = (email, password) => {
    cy.server();
    cy.route({
        method: 'POST',
        url: 'https://development-filerequest.newlinetechno.net/login'
    }).as('login');

    email && cy.get('#username').type(email);
    password && cy.get('#password').type(password);
    cy.get('.btn').click()
};

require('@4tw/cypress-drag-drop');

// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
