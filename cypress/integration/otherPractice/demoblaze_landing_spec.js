describe('1.Navigate to the Homepage', () => {
    beforeEach(() =>
        cy.visit(Cypress.env("baseUrl") + ('/index.html'))

    );

    it('1.1.Check all elements in the Header', () => {
        cy.get('#nava').should("be.visible")
            .and("have.attr", 'href', 'index.html');
        cy.get('#navbarExample').should("be.visible");
        cy.contains('Home').should("be.visible");
        cy.get(':nth-child(2) > .nav-link').should("be.visible")
            .and("have.text", 'Contact');
        cy.get(':nth-child(3) > .nav-link').should("be.visible")
            .and("have.text", "About us");
        cy.contains('Cart').should("be.visible");
        cy.get('#cartur').should("be.visible")
            .and("have.text", 'Cart');
        cy.get('#login2').should("be.visible")
            .and("have.text", "Log in");
        cy.get('#signin2').should("be.visible")
            .and("have.text", "Sign up");
    });

    it('1.2. The categories section should be displayed', () => {
        cy.get('#cat').should("be.visible").and("have.text", "CATEGORIES");
        cy.contains('Phones').should("be.visible");
        cy.contains('Laptops').should("be.visible");
        cy.contains('Monitors').should("be.visible");
    });

    it('1.3.The image should be displayed', () => {
        cy.get('.active > .d-block').should("have.attr", "src", "Samsung1.jpg");
        cy.get('.carousel-control-next-icon').click();
        cy.get('.active > .d-block').should("have.attr", "src", "nexus1.jpg");
        cy.get('.carousel-control-next-icon').click();
        cy.get('.active > .d-block').should("have.attr", "src", "iphone1.jpg");
    });

    it('1.4. The cards should be displayed in the catalogue.', () => {
        cy.clearCookies();
        cy.get('div.card.h-100').should("have.length", 9);
        cy.get('#next2').click();
        cy.get('div.card.h-100').should("have.length", 6);
        cy.get('#prev2').click();
    });

    it('1.5. The footer should be visible', () => {
        cy.get('#footc').should("be.visible");
        cy.get(".thumbnail").should("be.visible").and("have.length", 3);
        cy.get('#fotcont > :nth-child(1)').should("have.text", '\n        \n          \n            About Us\n            We believe performance needs to be validated at every stage of the software development cycle and our\n              open source compatible, massively scalable platform makes that a reality.\n          \n        \n      ');
        cy.get('.col-sm-3').should("have.text", "\n        \n          \n            Get in Touch\n            Address: 2390 El Camino Real\n            Phone: +440 123456\n            Email: demo@blazemeter.com \n          \n        \n      ");
        cy.get(':nth-child(3) > .thumbnail > .caption > h4').should("have.text", ' PRODUCT STORE');
        cy.get('h4 > img').should("have.attr", "src", "bm.png")
    });

    it('1.6. Call the Contact window and check form', () => {
        cy.get(':nth-child(2) > .nav-link').click();
        cy.get('#exampleModal > .modal-dialog > .modal-content').should("be.visible");
        cy.get('#recipient-email').should("be.empty");
        cy.get('#recipient-name').should("be.empty");
        cy.get('#message-text').should("be.empty");
        cy.contains('Send message').should("be.enabled");
        cy.get('button.btn.btn-secondary').should("be.enabled");
    });

    it('1.7. User should fill out the table.', () => {
        cy.get(':nth-child(2) > .nav-link').click();
        cy.get('#exampleModal > .modal-dialog > .modal-content').should("be.visible");
        cy.get("#recipient-email").type('some@mail.com');
        cy.get("#recipient-name").type('someName');
        cy.get('#message-text').type('the some text');
        cy.contains('Send message').click();
    });

    it('1.8. Check the form  after closing.', () => {
        cy.get(':nth-child(2) > .nav-link').click();
        cy.get('#exampleModal > .modal-dialog > .modal-content').should("be.visible");
        cy.get("#recipient-email").type('some@mail.com');
        cy.get("#recipient-name").type('someName');
        cy.get('#message-text').type('the some text');
        cy.contains('Close').click();
        cy.wait(1000);
        cy.get(':nth-child(2) > .nav-link').click();
        cy.wait(1000);
        cy.get('#recipient-email').should("be.empty");
        cy.get('#recipient-name').should("be.empty");
        cy.get('#message-text').should("be.empty");
    });

    it('1.9. Check the form  after closing.', () => {
        cy.get(':nth-child(2) > .nav-link').click();
        cy.contains('Send message').click();
    });

    it('1.10. Send message with empty form.', () => {
        cy.get(':nth-child(3) > .nav-link').click();
        cy.get('.modal-content').should("be.visible");
        cy.get('#example-video_html5_api').click({force:true});
    })
});
