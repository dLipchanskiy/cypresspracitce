describe('cart page', () => {
    beforeEach(() =>
        cy.visit(Cypress.env("baseUrl") + ('/index.html'))
    );

    it('2.1.Purchase card.', () => {
        cy.get('#cartur').click();
        cy.url().should("include", "/cart.html");
        cy.get('.btn.btn-success').click();
        cy.get('#name').type('someName');
        cy.get('#country').type('someCountry');
        cy.get('#city').type('someCity');
        cy.get('#card').type('456478971231');
        cy.get('#month').type('08');
        cy.get('#year').type('22');
        cy.contains('Purchase').click();
        cy.get('.lead').should("be.visible")
    });

    it('2.2. Purchase card with empty fields.', () => {
        cy.get('#cartur').click();
        cy.url().should("include", "/cart.html");
        cy.get('.btn.btn-success').click();
        cy.wait(1000);
        cy.get('#name').should("be.empty");
        cy.get('#country').should("be.empty");
        cy.get('#city').should("be.empty");
        cy.get('#card').should("be.empty");
        cy.get('#month').should("be.empty");
        cy.get('#year').should("be.empty");
        // cy.get(alert()).should("be.visible")
    });

});
