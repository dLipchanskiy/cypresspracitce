describe('1.Landing page', () => {
    beforeEach(() =>
        cy.visit("http://litecart.stqa.ru/en/")
    );

    it("1.2.Litecart site", () => {
        cy.get('[id="logotype-wrapper"]').should("be.visible");
        cy.get('#region').should("be.visible");
        cy.get('.currency').should("have.text", "USD");
        cy.get('.fancybox-region').should("have.text", "Change");
        cy.contains('Change').click();
        cy.get('#fancybox-content').should("be.visible");
        cy.get('#fancybox-overlay').should("be.visible")
    });

    it('1.3.Check all elements in the Regional setting', () => {
        cy.contains('Change').click();
        cy.contains('English').should("have.text", "English");
        cy.contains('US Dollars').should("have.text", "US Dollars");
        cy.contains("Russian Federation").should("have.text", "Russian Federation");
        cy.contains('Save').click({force: true});
    });


    it('1.4.Search field with valid data', () => {
        cy.get('.input-wrapper').type("Duck {enter}");
        cy.get('#box-search-results > .title')
            .should("be.visible")
            .and("have.text", 'Search Results for "Duck"');
        cy.get('#box-search-results .product').should("have.length", 5);
    });

    it('1.5.Search field with invalid data', () => {
        cy.get('.input-wrapper').type("Men{enter}");
        cy.get('#box-search-results > .title').should("have.text", 'Search Results for "Men"');
        cy.get('.input-wrapper > input').clear();
        cy.contains('Rubber Ducks').click();
        cy.get('#logotype-wrapper').click();
        cy.get('.input-wrapper').should("have.value", '');
    });
    it('1.6.Search field with empty field', () => {
        cy.get('.input-wrapper').type("{enter}");
        cy.get('#box-search-results > .title').should("have.text", 'All Products');
        cy.get('#box-search-results .product').should("have.length", 6);
    });

    it('1.6.should be displayed elements in the categories', () => {
        cy.get('#box-most-popular .product').should("be.visible").and("have.length", 6);
        cy.get('#box-campaigns .product').should("be.visible").and("have.length", 1);
        cy.get('#box-latest-products .product').should("be.visible").and("have.length", 6);
    });

    it('1.7.The image should be displayed', () => {
        cy.get('#rslides1_s0 > a > img').should("be.visible")
            .and("have.attr", "src", "/images/slides/1-lonely-duck.jpg");
        cy.get('#box-logotypes').should("be.visible");
    });

    it('Should redirect user to the category', () => {
        cy.get('.list-vertical > .category-1 > a').should("have.text", "Rubber Ducks").click();
        cy.url().should("include", 'rubber-ducks-c-1/');
        cy.get('.list-vertical > .category-2 > a').should("have.text", "Subcategory");
        cy.get('.list-vertical > .category-4 > a').should("have.text", "Ducks");
        cy.get('.list-vertical > .category-6 > a').should("have.text", "Popular ducks");
    })

});
