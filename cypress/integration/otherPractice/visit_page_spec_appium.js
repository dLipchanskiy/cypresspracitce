/// <reference types="Cypress" />

describe('1.Landing page' , () => {
    beforeEach(() =>
        cy.visit('https://www.cypress.io/')
    );
    it('1.2.A ll elements should be displayed', () =>{
        cy.get('.tagLine > div')
            .should("be.visible")
            .and("have.text", "The web has evolved.Finally, testing has too.");
        cy.get('.install-download')
            .should("be.visible")
            .and("have.attr", 'href', "https://download.cypress.io/desktop");
        cy.get('.HeroEpilogue__PromoVideo-sc-139vtzr-2')
            .should("be.visible");
        cy.get(".cookieConsent")
            .should("be.visible");
        cy.get('.button')
            .should("be.enabled")
            .and("have.text", "Got it!");
        cy.get('[data-cy=top-banner]')
            .should("be.visible");
        cy.get('.TopBannersItem__Body-sc-1fxq0v9-1 > .blue')
            .should("be.visible")
            .and("have.text", "Register");
        cy.contains('Test runner')
            .should("be.visible")
            .and("have.attr","href", "/features");
        cy.contains('Dashboard Service')
            .should("be.visible")
            .and("have.attr", "href", "/dashboard");
        cy.contains("What sets Cypress apart?")
            .should("be.visible");
        cy.get('.text-center > .large')
            .should("be.visible")
            .and("have.text", "See more features");
        cy.get('.TestCode__Wrapper-sc-6g43n2-0 > .container > :nth-child(1) > .text-center ' +
            '> .SectionTitle__Heading-uwawm7-0')
            .should("be.visible")
            .and("have.text", "Test your code, not your patience.");
        cy.get('.TestCode__InlineFlex-sc-6g43n2-1 > .large')
            .should("be.visible")
            .and("have.text", "Check out the code");
        cy.get('#testimonials > .container > :nth-child(1) > .text-center > .non-separated')
            .should("be.visible").and("have.text", "Do you  Cypress? Say it!");
        cy.contains("Say it!")
            .should("be.visible")
            .and("have.attr", "href", "https://twitter.com/intent/tweet?text=Check+out+%40Cypress_io+for+end+to+end+%23testing&url=https%3A%2F%2Fcypress.io");
        cy.get('.TwitterTestimonials__Epilogue-sc-18sgatg-3')
            .should("be.visible")
            .and("have.text", "Read more about why people love Cypress here.");
        cy.get('.TwitterTestimonials__Epilogue-sc-18sgatg-3 > .Link__StyledLink-sc-5cc5in-0')
            .should("have.text", "here.")
            .and("have.attr", "href", "/testimonials");
    });

    it("1.3.Check elements", () => {
        cy.get()
    })
});
