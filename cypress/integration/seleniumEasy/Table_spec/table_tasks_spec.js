describe('Table tasks', () => {
    beforeEach( () => {
        cy.visit('https://www.seleniumeasy.com/test/table-search-filter-demo.html')
    });

    it('The table should be visible', () => {
        cy.url().should("include", '/test/table-search-filter-demo.html');
        cy.get(':nth-child(2) > .panel').should("be.visible");
        cy.get('#task-table-filter').should("be.empty");
        cy.get('.clickable > .glyphicon').should("be.visible");
        cy.get('#task-table > thead > tr ')
            .should("have.text", '\n\t\t\t\t\t\t\t\t#\n\t\t\t\t\t\t\t\tTask\n\t\t\t\t\t\t\t\tAssignee\n\t\t\t\t\t\t\t\tStatus\n\t\t\t\t\t\t\t');
        cy.get('#task-table > tbody > tr').should("have.length", 7)
    });

    it('The user should be enter a valid data in the search field', () => {
        cy.get('#task-table-filter').type("tra");
        cy.get('#task-table > tbody > :nth-child(4) > :nth-child(1)')
            .should("have.text", "4");
        cy.get('#task-table > tbody > :nth-child(4) > :nth-child(2)')
            .should("have.text", "Bootstrap 3");
        cy.get('#task-table > tbody > :nth-child(4) > :nth-child(3)')
            .should("have.text", "Emily John");
        cy.get('#task-table > tbody > :nth-child(4) > :nth-child(4)')
            .should("have.text", "in progress");
    });

    it('The user should be enters invalid data in the search field', () => {
        cy.get('#task-table-filter').type("dajidaji");
        cy.get('.filterTable_no_results > td')
            .should("have.text", "No results found");
    });

    it('The Listed user table should be visible and filters should be disabled', () => {
        cy.get(':nth-child(3) > .panel').should("be.visible");
        cy.get('.btn').should("be.visible")
            .and("be.enabled");
        cy.get(':nth-child(1) > .form-control')
            .should("be.disabled");
        cy.get('.filters > :nth-child(2) > .form-control')
            .should("be.disabled");
        cy.get(':nth-child(3) > .form-control')
            .should("be.disabled");
        cy.get(':nth-child(4) > .form-control')
            .should("be.disabled");
    })

    it('The user should be turn on filters via press the filter button.', () => {
        cy.get('.btn').click();
        cy.get(':nth-child(1) > .form-control')
            .should("be.enabled");
        cy.get('.filters > :nth-child(2) > .form-control')
            .should("be.enabled");
        cy.get(':nth-child(3) > .form-control')
            .should("be.enabled");
        cy.get(':nth-child(4) > .form-control')
            .should("be.enabled");
    });

    it('The user should be enters a few symbols for search.', () => {
        cy.get('.btn').click();
        cy.get(':nth-child(1) > .form-control').type('4');
        cy.get(':nth-child(3) > .panel > .table > tbody > :nth-child(4) > :nth-child(1)')
            .should("have.text", '4');
        cy.get(':nth-child(3) > .panel > .table > tbody > :nth-child(4) > :nth-child(2)')
            .should("have.text", "mikesali");
        cy.get(':nth-child(3) > .panel > .table > tbody > :nth-child(4) > :nth-child(3)')
            .should("have.text", "Byron");
        cy.get(':nth-child(3) > .panel > .table > tbody > :nth-child(4) > :nth-child(4)')
            .should("have.text", "Kathaniko");
    });

    it('The user enters invalid characters in the search field', () => {
        cy.get('.btn').click();
        cy.get(':nth-child(1) > .form-control').type('adada');
        cy.get('.no-result > td').should("have.text", "No result found");
    })

});
