describe('Filter Records page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/table-records-filter-demo.html')
    });

    it('The user should be navigate to the Filter Records page.', () => {
        cy.url().should("include", "/test/table-records-filter-demo.html");
        cy.get('.tablefilter > .panel > .panel-body').should("be.visible");
        cy.get('.btn-success').should("be.enabled")
            .and("have.css", "background-color")
            .and("eq", "rgb(92, 184, 92)");
        cy.get('.btn-success').should("have.css", "border-color")
            .and("eq", "rgb(76, 174, 76)");
        cy.get('.btn-warning').should("be.enabled")
            .and("have.css", "background-color", "rgb(240, 173, 78)")
            .and("have.css", "border-color", "rgb(238, 162, 54)");
        cy.get('.btn-danger').should("be.enabled")
            .and("have.css", "background-color", "rgb(217, 83, 79)")
            .and("have.css", "border-color", "rgb(212, 63, 58)");
        cy.get('.btn-default').should("be.enabled")
    });

    it('The user should be click on the filter buttons.', () => {
        cy.get('.btn-success').click();
        cy.wait(1000);
        cy.get("[data-status='pagado']").should("be.visible");
        cy.get('.btn-warning').click();
        cy.wait(1000);
        cy.get("[data-status='pendiente']");
        cy.get('.btn-danger').click();
        cy.wait(1000);
        cy.get("[data-status='cancelado']").should("be.visible");
        cy.get('.btn-default').click();
        cy.wait(1000);
        cy.get("[data-status]").should("be.visible").and("not.have.css", "display-none")
    });
});
