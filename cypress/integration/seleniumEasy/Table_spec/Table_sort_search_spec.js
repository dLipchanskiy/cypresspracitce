describe('Table sort and search', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/table-sort-search-demo.html')
    });

    it('The user should be navigate to the page.', () => {
        cy.url().should("include", '/test/table-sort-search-demo.html');
    });

    it('The table should be visible and buttons should be active', () => {
        cy.get('#example_wrapper').should("be.visible");
        cy.get('select').should("have.value", "10");
        cy.get('input').should("be.empty");
        cy.get('#example_info').should("be.visible");
        cy.get('#example_previous').should("be.visible")
            .and("have.class", "disabled")
            .and("have.text", "Previous");
        cy.get('#example_next').should("be.visible")
            .and("have.text", "Next");
        cy.get('.current').should("have.text", "1");
    });

    it('The user should be sorting Name by asc', () => {
        cy.get('.sorting_asc').click();
        cy.get('#example > tbody > tr:nth-child(1) > td.sorting_1')
            .should("have.text", "Y. Berry");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "Chief Marketing Officer (CMO)");
        cy.get('tbody > :nth-child(1) > :nth-child(3)')
            .should("have.text", "New York");
        cy.get('tbody > :nth-child(1) > :nth-child(4)')
            .should("have.text", "40");
        cy.get('[data-order="1245884400"]')
            .should("have.text", "Thu 25th Jun 09");
        cy.get('[data-order="675000"]')
            .should("have.text", "$675,000/y");
    });

    it('The user should be sorting Name by desc', () => {
        cy.get('.sorting_asc').dblclick();
        cy.get('#example > tbody > tr:nth-child(1) > td.sorting_1')
            .should("have.text", "A. Cox");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "Junior Technical Author");
        cy.get('tbody > :nth-child(1) > :nth-child(3)')
            .should("have.text", "San Francisco");
        cy.get('tbody > :nth-child(1) > :nth-child(4)')
            .should("have.text", "66");
        cy.get('[data-order="1231718400"]')
            .should("have.text", "Mon 12th Jan 09");
        cy.get('[data-order="86000"]')
            .should("have.text", "$86,000/y");
    });

    it('The user should be sorting Position by asc', () => {
        cy.contains('Position').click();
        cy.get('[data-search="Airi Satou"]')
            .should("have.text", "A. Satou");
        cy.get(':nth-child(1) > .sorting_1')
            .should("have.text", "Accountant");
        cy.get('tbody > :nth-child(1) > :nth-child(3)')
            .should("have.text", "Tokyo");
        cy.get('tbody > :nth-child(1) > :nth-child(4)')
            .should("have.text", "33");
        cy.get('[data-order="1227830400"]')
            .should("have.text", "Fri 28th Nov 08");
        cy.get('[data-order="162700"]')
            .should("have.text", "$162,700/y");
    });

    it('The user should be sorting Position by desc', () => {
        cy.get('#example > thead > tr > th').contains('Position').dblclick();
        cy.get('[data-search="Gloria Little"]')
            .should("have.text", "G. Little");
        cy.get(':nth-child(1) > .sorting_1')
            .should("have.text", "Systems Administrator");
        cy.get('tbody > :nth-child(1) > :nth-child(3)')
            .should("have.text", "New York");
        cy.get('tbody > :nth-child(1) > :nth-child(4)')
            .should("have.text", "59");
        cy.get('[data-order="1239318000"]')
            .should("have.text", "Fri 10th Apr 09");
        cy.get('[data-order="237500"]')
            .should("have.text", "$237,500/y")
    });

    it('The user should be sorting Office by asc', () => {
        cy.get('#example > thead > tr > th').contains('Office').click();
        cy.get('[data-search="Cedric Kelly"]')
            .should("have.text", "C. Kelly");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "Senior Javascript Developer");
        cy.get(':nth-child(1) > .sorting_1')
            .should("have.text", "Edinburgh");
        cy.get('tbody > :nth-child(1) > :nth-child(4)')
            .should("have.text", "22");
        cy.get('[data-order="1332975600"]')
            .should("have.text", "Thu 29th Mar 12");
        cy.get('[data-order="433060"]')
            .should("have.text", "$433,060/y");
    });

    it('The user should be sorting Office by desc', () => {
        cy.get('#example > thead > tr > th').contains('Office').dblclick();
        cy.get('[data-search="Airi Satou"]')
            .should("have.text", "A. Satou");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "Accountant");
        cy.get(':nth-child(1) > .sorting_1')
            .should("have.text", "Tokyo");
        cy.get('tbody > :nth-child(1) > :nth-child(4)')
            .should("have.text", "33");
        cy.get('[data-order="1227830400"]')
            .should("have.text", "Fri 28th Nov 08");
        cy.get('[data-order="162700"]')
            .should("have.text", "$162,700/y");
    });

    it('The user should be sorting Age by asc', () => {
        cy.get('#example > thead > tr > th').contains('Age').click();
        cy.get('[data-search="Tatyana Fitzpatrick"]')
            .should("have.text", "T. Fitzpatrick");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "Regional Director");
        cy.get('tbody > :nth-child(1) > :nth-child(3)')
            .should("have.text", "London");
        cy.get(':nth-child(1) > .sorting_1')
            .should("have.text", "19");
        cy.get('[data-order="1268784000"]')
            .should("have.text", "Wed 17th Mar 10");
        cy.get('[data-order="385750"]')
            .should("have.text", "$385,750/y");
    });

    it('The user should be sorting Age by desc', () => {
        cy.get('#example > thead > tr > th').contains('Age').dblclick();
        cy.get('[data-search="Ashton Cox"]')
            .should("have.text", "A. Cox");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "Junior Technical Author");
        cy.get('tbody > :nth-child(1) > :nth-child(3)')
            .should("have.text", "San Francisco");
        cy.get(':nth-child(1) > .sorting_1')
            .should("have.text", "66");
        cy.get('[data-order="1231718400"]')
            .should("have.text", "Mon 12th Jan 09");
        cy.get('[data-order="86000"]')
            .should("have.text", "$86,000/y");
    });

    it('The user should be sorting Start Date by asc', () => {
        cy.get('#example > thead > tr > th').contains('Start Date').click()
        cy.get('[data-search="Charde Marshall"]')
            .should("have.text", "C. Marshall");
        cy.get('tbody > :nth-child(1) > :nth-child(2)')
            .should("have.text", "")
    })
});
