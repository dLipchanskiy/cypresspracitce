describe('Table Pagination page', () => {
    beforeEach( () => {
        cy.visit('https://www.seleniumeasy.com/test/table-pagination-demo.html')
    });

    it('The table should be displayed and pagination should work', () => {
        cy.get('.table-responsive').should("be.visible");
        cy.get('.btn-primary > tr > th').should("have.length", 7);
        cy.get('#myTable > tr').should("be.visible").and("have.css", "display",'table-row').and("have.length", 13)
        cy.get('.page_link').contains('2').click();
        cy.get('.page_link').contains('3').click();
    })
});
