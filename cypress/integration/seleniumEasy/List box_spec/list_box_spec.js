describe('List bot page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/bootstrap-dual-list-box-demo.html')
    });

    it('Check all elements on the page', () => {
        cy.get('.list-left').should("be.visible");
        cy.get('.list-left > .well > .list-group > .list-group-item').should("have.length", 5);
        cy.get('.list-left .input-group > .form-control').should("be.empty");
        cy.get('.list-left > .well > #listhead .btn').should("be.visible");
        cy.get('.list-arrows').should("be.visible");
        cy.get('.move-left').should("be.visible");
        cy.get('.move-right').should("be.visible");
        cy.get('.list-right').should("be.visible");
        cy.get('.list-right > .well > .list-group > .list-group-item').should("have.length", 5);
        cy.get('.list-right .input-group > .form-control').should("be.empty");
        cy.get('.list-right > .well > #listhead .btn').should("be.visible");
    });

    it('The user move the option from the left panel to the right panel ', () => {
        cy.get('.list-left > .well > .list-group > :nth-child(1)').click();
        cy.get('.move-right').click();
        cy.get('.list-right > .well > .list-group > .list-group-item').should("have.length", 6);
        cy.get('.list-left > .well > .list-group > .list-group-item').should("have.length", 4);
    });

    it('The user move the option from the right panel to the left panel ', () => {
        cy.get('.list-right > .well > .list-group > :nth-child(1)').click();
        cy.get('.move-left').click();
        cy.get('.list-right > .well > .list-group > .list-group-item').should("have.length", 4);
        cy.get('.list-left > .well > .list-group > .list-group-item').should("have.length", 6);
    });

    it('The user move all options to the right panel', () => {
        cy.get('.list-left > .well > #listhead .btn').click();
        cy.get('.move-right').click();
        cy.get('.list-right > .well > .list-group > .list-group-item').should("have.length", 10);
        cy.get('.list-left > .well > .list-group > .list-group-item').should("have.length", 0);
    })
});
