describe('JQuery dual list page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/jquery-dual-list-box-demo.html')
    });

    it('Check all elements on the page', () => {
        cy.get('.col-md-6 > .panel').should("be.visible");
        cy.get('#pickList').should("be.visible");
        cy.get('.form-control.pickListSelect.pickData').find('option')
            .should("have.length", 15);
        cy.get(".form-control.pickListSelect.pickListResult").should("be.visible")
            .and("be.empty");
        cy.get('.col-sm-2').should("be.visible");
        cy.get('.pAdd').should("be.visible").and("be.enabled")
            .and("have.text", "Add");
        cy.get('.pAddAll').should("be.visible").and("be.enabled")
            .and("have.text", "Add All");
        cy.get('.pRemove').should("be.visible").and("be.enabled")
            .and("have.text", "Remove");
        cy.get('.pRemoveAll').should("be.visible").and("be.enabled")
            .and("have.text", "Remove All");
    });

    it('The user move the options', () => {
        cy.get('[data-id="1"]').click();
        cy.get('[data-id="2"]').click();
        cy.get('[data-id="3"]').click();
        cy.get('[data-id="7"]').click();
    });
});
