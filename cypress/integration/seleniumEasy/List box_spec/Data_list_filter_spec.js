describe('The user navigate to the page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/data-list-filter-demo.html');
    });

    it('Check all elements on the page', () => {
        cy.url().should("include", '/test/data-list-filter-demo.html');
        cy.get('.text-left').should("be.visible");
        cy.get('#input-search').should("be.visible")
            .and("be.empty")
            .and("have.attr", "placeholder", "Search Attendees...");
        cy.get('.info-block').should("have.length", 6)
            .and("be.visible");
    });

    it('The user searching exist contact', () => {
        cy.get('#input-search').type('Tyreese Burn');
        cy.get('.col-sm-6').contains('Tyreese Burn');
    });

    it('The user searching non-exist contact', () => {
        cy.get('#input-search').type('hasdjas;d');
        cy.get('.searchable-container').should("not.be.visible");
    })
});
