describe('inputForm_spec', () => {
    beforeEach( () => {
        cy.visit('https://www.seleniumeasy.com/test/ajax-form-submit-demo.html')
    });

    it('The form should be empty and button should be active', () => {
        cy.url().should("include", "/test/ajax-form-submit-demo.html");
        cy.get('.text-center > .row > .col-md-6').should("be.visible");
        cy.get('.col-md-6 > .panel').should("be.visible");
        cy.get('#title').should("be.empty");
        cy.get('#description').should("be.empty");
        cy.get('#btn-submit').should("be.enabled")
            .and("have.value", "submit");
    });

    it('Submit the form with empty fields', () => {
        cy.get('#btn-submit').click();
        cy.get('.title-validation').should("be.visible");
        cy.get('#title').should("be.visible")
            .and("have.css", "border-color").and("eq", 'rgb(255, 0, 0)');
    });

    it('The user should be send the form', () => {
        cy.get('#title').type("Test Name");
        cy.get('#description').type("Test description");
        cy.get('#btn-submit').click();
        cy.get('#submit-control').should("have.text", "Form submited Successfully!")
    })
});
