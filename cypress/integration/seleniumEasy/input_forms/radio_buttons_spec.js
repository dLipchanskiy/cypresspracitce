describe('Radio-buttons page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/basic-radiobutton-demo.html')
    });

    it('Radio button demo page should be empty', () => {
        cy.url().should("include", '/test/basic-radiobutton-demo.html');
        cy.get(':nth-child(4) > .panel-heading').should("be.visible");
        cy.get('input[type="radio"]').should("not.be.checked");
        cy.get('.panel-body > :nth-child(2) > input').should("not.be.checked");
        cy.get('.panel-body > :nth-child(3) > input').should("not.be.checked");
        cy.get('#buttoncheck').should("be.visible")
            .and("be.enabled");
        cy.get('#buttoncheck').click();
        cy.get('.radiobutton').should("be.visible")
            .and("have.text", "Radio button is Not checked");
    });

    it('Radio button page should be checked (Male).', () => {
        cy.get('input[type="radio"]').first().check('Male').should("be.checked");
        cy.get('#buttoncheck').click();
        cy.get('.radiobutton').should("have.text", "Radio button 'Male' is checked");
    });

    it('Radio button page should be checked (Female).', () => {
        cy.get('.panel-body > :nth-child(3) > input').check().should("be.checked");
        cy.get('#buttoncheck').click();
        cy.get('.radiobutton').should("have.text", "Radio button 'Female' is checked");
    });

    it('Group radio buttons Demo form should be empty and unchecked', () => {
        cy.get('.panel-body > :nth-child(2) > :nth-child(2) > input').click().should("be.checked");
        cy.get('.panel-body > :nth-child(2) > :nth-child(3) > input').click().should("be.checked");
        cy.get(':nth-child(3) > :nth-child(2) > input').click().should("be.checked");
        cy.get(':nth-child(3) > :nth-child(3) > input').click().should("be.checked");
        cy.get(':nth-child(4) > input').click().should("be.checked");
        cy.get('.panel-body > .btn').click();
        cy.get('.groupradiobutton').should("have.text", "Sex : Female Age group: 15 - 50")
    });

});



