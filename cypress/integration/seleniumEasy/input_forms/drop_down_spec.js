describe('Drop-down page', () => {
   beforeEach( () => {
       cy.visit('https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html')
   });

    it(' The user should be visit to page.', () => {
      cy.url().should("include", "/test/basic-select-dropdown-demo.html");
        cy.get(':nth-child(4) > .panel-body').should("be.visible");
        cy.get('#select-demo').should("be.visible")
            .and("have.value", null);
    });

    it('The user should be select any value', () => {
        cy.get('#select-demo').select('Sunday');
        cy.get('.selected-value').should("have.text", "Day selected :- Sunday");
        cy.get('#select-demo').select('Monday');
        cy.get('.selected-value').should("have.text", "Day selected :- Monday");
        cy.get('#select-demo').select('Tuesday');
        cy.get('.selected-value').should("have.text", "Day selected :- Tuesday");
        cy.get('#select-demo').select('Wednesday');
        cy.get('.selected-value').should("have.text", "Day selected :- Wednesday");
        cy.get('#select-demo').select('Friday');
        cy.get('.selected-value').should("have.text", "Day selected :- Friday");
        cy.get('#select-demo').select('Saturday');
        cy.get('.selected-value').should("have.text", "Day selected :- Saturday");
        cy.get('#select-demo').select('Thursday');
        cy.get('.selected-value').should("have.text", "Day selected :- Thursday");
    });

    it('The form should be have cities and buttons', () => {
        cy.get(':nth-child(5) > .panel-body').should("be.visible");
        cy.get('#multi-select').should("be.visible");
        cy.get('[value="California"]').should("be.visible");
        cy.get('[value="Florida"]').should("be.visible");
        cy.get('[value="New Jersey"]').should("be.visible");
        cy.get('[value="Ohio"]').should("be.visible");
        cy.get('[value="Texas"]').should("be.visible");
        cy.get('[value="Pennsylvania"]').should("be.visible");
        cy.get('[value="Washington"]').should("be.visible");
        cy.get('#printMe').should("be.enabled")
            .and("be.visible")
            .and("have.text", "First Selected");
        cy.get('#printAll').should("be.enabled")
            .and("be.visible")
            .and("have.text", "Get All Selected");
        cy.get('#printMe').click();
        cy.get('.getall-selected')
            .should("have.text", "First selected option is : undefined");
        cy.get('#printAll').click();
        cy.get('.getall-selected')
            .should("have.text", 'Options selected are : ');
    });

    it('The user should be selected any value', () => {
       cy.get('[value= "New Jersey"]').click();
       cy.get('#printMe').click();
       cy.get('.getall-selected')
           .should("have.text", "First selected option is : New Jersey");
       cy.get('#printAll').click();
        cy.get('.getall-selected')
            .should("have.text", "Options selected are : New Jersey");
    });
});
