describe('Checkbox page ', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/basic-checkbox-demo.html')
    });

    it('Uncheck checkboxes' , () => {
        cy.url().should("include", '/test/basic-checkbox-demo.html');
        cy.get('.col-md-6 > :nth-child(4)').should("be.visible");
        cy.get('#isAgeSelected[type="checkbox"]').should("have.value", '');
    });

    it('The user should be checked the checkbox', () => {
        cy.get('#isAgeSelected[type="checkbox"]').check();
        cy.get('#txtAge').should("have.text", "Success - Check box is checked");
    });

    it('The Multiple Checkbox Demo should be unchecked.', () => {
        cy.get('.col-md-6 > :nth-child(5)').should("be.visible");
        cy.get('.cb1-element[type="checkbox"]').should("have.value", '');
        cy.get('#check1').should("be.enabled").and("have.value", 'Check All');
    });

    it('Check all checkboxes.', () => {
        cy.get('.cb1-element[type="checkbox"]').check();
        cy.get('.cb1-element[type="checkbox"]').should("be.checked" );
        cy.get('#check1').should("have.value", 'Uncheck All');
        cy.get('#check1').click();
        cy.get('.cb1-element[type="checkbox"]').should("have.value", '');
    });
});
