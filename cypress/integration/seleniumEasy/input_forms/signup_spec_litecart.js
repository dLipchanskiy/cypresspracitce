describe('1.Landing page', () => {
    beforeEach(() =>
        cy.visit("http://litecart.stqa.ru/en/")
    );
    it('should create account',() => {
        cy.contains('New customers click here').click();
        cy.url().should("include", 'http://litecart.stqa.ru/en/create_account');
        cy.get('.title').should("have.text", "Create Account");
        cy.get(':nth-child(1) > :nth-child(1) > input').type('someID');
        cy.get('tbody > :nth-child(1) > :nth-child(2) > input').type('someCompany');
        cy.get('tbody > :nth-child(2) > :nth-child(1) > input').type('someName');
        cy.get(':nth-child(2) > :nth-child(2) > input').type('someLastName');
        cy.get(':nth-child(3) > :nth-child(1) > input').type('firstAddress');
        cy.get(':nth-child(4) > :nth-child(1) > input').type('611123');
        cy.get(':nth-child(3) > :nth-child(2) > input').type('SomeSecondAddress');
        cy.get(':nth-child(4) > :nth-child(2) > input').type('someCity');
        cy.get(':nth-child(6) > :nth-child(1) > input').type('some@mail.com');
        cy.get(':nth-child(6) > :nth-child(2) > input').type('+380931233761');
        cy.get(':nth-child(8) > :nth-child(1) > input').type('Asdasd123@');
        cy.get(':nth-child(8) > :nth-child(2) > input').type('Asdasd123@');
        cy.get('.select2-selection__arrow').click({force:true});
        cy.get('.select2-selection__arrow').click();
        cy.get('td > button').click();
        cy.get('.notice')
            .should("have.text", ' The email address already exists in our customer database. Please login or select a different email address.')
    })
});
