describe('The drop down search', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/jquery-dropdown-search-demo.html')
    });

    it('The user should be select country', () => {
        cy.url().should("include", '/test/jquery-dropdown-search-demo.html');
        cy.get(':nth-child(2) > .panel').should("be.visible");
        cy.get(':nth-child(3) > .panel').should("be.visible");
        cy.get(':nth-child(4) > .panel').should("be.visible");
        cy.get(':nth-child(5) > .panel').should("be.visible");
        cy.get('h2').should("have.text", "Single Select - Search and Select country");
        cy.get(':nth-child(2) > .panel > .panel-body > .select2 > .selection > .select2-selection')
            .should("be.visible");
        cy.get('.select2-search').should("be.visible");
        cy.get(':nth-child(2) > .panel > .panel-body > .select2 > .selection > .select2-selection')
            .click();
        cy.get('#select2-country-results > :nth-child(4)')
            .click();
        cy.get('#select2-country-container').should("have.text", "Denmark");
    });

    it('The user should be select multiple values', () => {
        cy.get(':nth-child(3) > .panel').should("be.visible");
        cy.get('.select2-search__field').should("be.empty");
        cy.get('.select2-search__field').type('Colorado{enter}');
        cy.get('.select2-search__field').type('Arizona{enter}');
        cy.get('[title="Arizona"]').should("have.text", "×Arizona");
        cy.get('[title="Colorado"]').should("have.text", "×Colorado");
    });

    it('The user should be disable values', () => {
        cy.get(':nth-child(4) > .panel > .panel-body > .select2 > .selection > .select2-selection > .select2-selection__arrow')
            .click();
        cy.get('.select2-dropdown > .select2-search > .select2-search__field').type("P{enter}");
        cy.get(':nth-child(4) > .panel > .panel-body > .select2 > .selection > .select2-selection')
            .should("have.text", "Puerto Rico")
    });

    it('The user should be select a file.', () => {
        cy.get(':nth-child(5) > .panel').should("be.visible");
        cy.get('#files').should("be.visible");
        cy.get('#files').select("Python");
        cy.get('#files').should("have.value", "jquery")
    })
});
