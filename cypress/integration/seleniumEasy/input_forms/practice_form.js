describe('Easy test site', () => {
    beforeEach(() =>
        cy.visit("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
    );

    it('The Single Input field should be empty', () => {
        cy.url().should("include", "/test/basic-first-form-demo.html");
        cy.get('.col-md-6 > :nth-child(4)').should("be.visible");
        cy.get('input[id="user-message"]').should("be.empty");
        cy.get('#get-input > .btn').should("be.enabled")
            .and("have.text", 'Show Message');
        cy.get('#display').should("be.empty");
    });

    it('Single Input Field', () => {
        cy.get('input[id="user-message"]').type('Cypress Test');
        cy.get('#get-input > .btn').click();
        cy.get('#display').should("have.text", 'Cypress Test');
    });

    it('The Two input fields form should be empty', () => {
        cy.get('.col-md-6 > :nth-child(5)').should("be.visible");
        cy.get('#sum1').should("be.empty");
        cy.get('#sum2').should("be.empty");
        cy.get('#gettotal > .btn').should("be.enabled");
        cy.get('#displayvalue').should("be.empty");
    });

    it('Two input Fields' , () => {
        cy.get('#sum1').type('1 Field');
        cy.get('#sum2').type('2 Field');
        cy.get('#gettotal > .btn').click();
        cy.get('#displayvalue').should("have.text", "3");
    });
});
