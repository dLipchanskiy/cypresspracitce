describe('Bootstrap modal page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/bootstrap-modal-demo.html')
    });

    it('Check all elements', () => {
        cy.url().should("include", "/test/bootstrap-modal-demo.html");
        cy.get('[href="#myModal0"]').contains('Launch modal').should("be.visible");
        cy.get(':nth-child(2) > .col-md-4 > .panel').should("be.visible");
        cy.get(':nth-child(3) > .col-md-4 > .panel').should("be.visible");
        cy.get('[href="#myModal"]').contains('Launch modal').should("be.visible");
    });

    it('Single modal example', () => {
        cy.get('[href="#myModal0"]').click();
        cy.get('#myModal0 > .modal-dialog').should("be.visible");
        cy.get('#myModal0 > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]')
            .should("be.visible").and("have.text", "Close");
        cy.get('#myModal0 > .modal-dialog > .modal-content > .modal-footer > .btn-primary')
            .should("be.visible").and("have.text", "Save changes");
    });

    it('The user call the single modal window and click on the "Cancel" button.', () => {
        cy.get('[href="#myModal0"]').click();
        cy.get('#myModal0 > .modal-dialog').should("be.visible");
        cy.get('#myModal0 > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]').click();
        cy.get('#myModal0 > .modal-dialog').should("not.be.visible");
        cy.get(':nth-child(2) > .col-md-4 > .panel').should("be.visible");
    });

    it('The user call the single modal window and click on the "Save" button', () => {
        cy.get('[href="#myModal0"]').click();
        cy.get('#myModal0 > .modal-dialog').should("be.visible");
        cy.get('#myModal0 > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click();
        cy.get('#myModal0 > .modal-dialog').should("not.be.visible");
        cy.get(':nth-child(2) > .col-md-4 > .panel').should("be.visible");
    });

    it('The user call the multiple modal window', () => {
        cy.get('[href="#myModal"]').click();
        cy.get('#myModal > .modal-dialog > .modal-content').should("be.visible");
        cy.get('.modal-body > .btn').should("be.visible")
            .and("have.text", "Launch modal");
        cy.get('#myModal > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]')
            .should("be.visible").and("have.text", "Close");
        cy.get('#myModal > .modal-dialog > .modal-content > .modal-footer > .btn-primary')
            .should("be.visible").and("have.text", 'Save changes');
    });

    it('The user call the multiple window and click on the "Close" button.', () => {
        cy.get('[href="#myModal"]').click();
        cy.get('#myModal > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]').click();
        cy.get('#myModal > .modal-dialog > .modal-content').should("not.be.visible");
        cy.get('.text-center > :nth-child(1) > .col-md-6').should("be.visible");
    });

    it('The user call the multiple window and click on the "Save" button.', () => {
        cy.get('[href="#myModal"]').click();
        cy.get('#myModal > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click();
        cy.get('#myModal > .modal-dialog > .modal-content').should("not.be.visible");
        cy.get('.text-center > :nth-child(1) > .col-md-6').should("be.visible");
    });

    it('The user call the multiple window  and click on the "Launch modal" button', () => {
        cy.get('[href="#myModal"]').click();
        cy.get('.modal-body > .btn').click();
        cy.get('#myModal2').should("be.visible");
        cy.get('#myModal2 > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]')
            .should("be.visible").and("have.text", "Close");
        cy.get('#myModal2 > .modal-dialog > .modal-content > .modal-footer > .btn-primary')
            .should("be.visible").and("have.text", "Save changes");
    });

    it('The multiple window and click on the "Close" button', () => {
        cy.get('[href="#myModal"]').click();
        cy.wait(300);
        cy.get('.modal-body > .btn').click();
        cy.wait(300);
        cy.get('#myModal2 > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]').click();
        cy.get('#myModal > .modal-dialog > .modal-content').should("be.visible");
        cy.get('#myModal > .modal-dialog > .modal-content > .modal-footer > [data-dismiss="modal"]').click();
        cy.get('#myModal > .modal-dialog > .modal-content').should("be.visible");
    });
});
