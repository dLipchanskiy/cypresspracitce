/// <reference types="cypress" />
describe('Bootstrap alerts page', () => {
    beforeEach(() => {
        cy.visit("https://www.seleniumeasy.com/test/bootstrap-progress-bar-dialog-demo.html")
    });

    it('Check all elements', () => {
        cy.get('.text-center > .row > .col-md-6').should("be.visible");
        cy.get('.btn-primary').should("be.visible")
            .and("be.enabled").and("have.text", "Show dialog")
            .and("have.css", "background-color").and("eq", "rgb(51, 122, 183)");
        cy.get('.btn-success').should("be.visible")
            .and("be.enabled").and("have.text", "Show dialog")
            .and("have.css", "background-color").and("eq", "rgb(92, 184, 92)");
        cy.get('.btn-warning').should("be.visible")
            .and("be.enabled").and("have.text", "Show dialog")
            .and("have.css", "background-color").and("eq", "rgb(240, 173, 78)");
    });

    it('The user click on the "Show dialog" button', () => {
        cy.get('.btn-primary').click();
        cy.get('.modal-content').should("be.visible");
        cy.get('.modal-header').should("have.text", "Loading");
        cy.get('.progress-bar').should("be.visible")
            .and("have.css", "background-color").and("eq", "rgb(51, 122, 183)");
    });

    it('The user click on the Green dialog button', () => {
        cy.get('.btn-success').click();
        cy.get('.modal-content').should("be.visible");
        cy.get('.modal-header').should("have.text", "Custom message");
        cy.get('.progress-bar').should("be.visible")
            .and("have.css", "background-color").and("eq", "rgb(51, 122, 183)");
    });

    it('The user click on the orange button', () => {
        cy.get('.btn-warning').click();
        cy.get('.modal-content').should("be.visible");
        cy.get('.modal-header').should("have.text", "Hello Mr. Alert !");
        cy.get('.progress-bar').should("be.visible")
            .and("have.css", "background-color").and("eq", "rgb(240, 173, 78)");
    })
});
