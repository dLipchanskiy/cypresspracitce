/// <reference types="cypress" />
describe('Bootstrap alerts page', () => {
    beforeEach(() => {
        cy.visit("https://www.seleniumeasy.com/test/generate-file-to-download-demo.html")
    });

    it('Check all elements on the page', () => {
        cy.get('.col-md-6 > .panel').should("be.visible");
        cy.get('#textbox').should("be.empty");
        cy.get('#textarea_feedback').should("have.text", "500 characters remaining");
        cy.get('#create').should("have.text", "Generate File");
    });

    it('the user enter data and download file', () => {
        cy.get('#textbox').type("Cypress Test");
        cy.get('#create').click();
        cy.get('#link-to-download').click();
    })
});
