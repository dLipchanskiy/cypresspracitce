/// <reference types="cypress" />
describe('Bootstrap alerts page', () => {
    beforeEach(() => {
        cy.visit("https://www.seleniumeasy.com/test/bootstrap-alert-messages-demo.html")
    });

    it('Check all elements', () => {
        cy.get('#autoclosable-btn-success').should("be.visible")
            .and("have.text", "\n              Autocloseable success message\n              ")
            .and("have.css", "background-color").and("eq", "rgb(92, 184, 92)");
        cy.get('#normal-btn-success').should("be.visible")
            .and("have.text", "\n              Normal success message\n              ")
            .and("have.css", "background-color").and("eq", "rgb(92, 184, 92)");
        cy.get('#autoclosable-btn-warning').should("be.visible")
            .and("have.text", "\n              Autocloseable warning message\n              ")
            .and("have.css", "background-color").and("eq", "rgb(240, 173, 78)");
        cy.get('#normal-btn-warning').should("be.visible")
            .and("have.text", "\n              Normal warning message\n              ")
            .and("have.css", "background-color").and("eq", "rgb(240, 173, 78)");
        cy.get("#autoclosable-btn-danger").should("be.visible")
            .and("have.text", "\n              Autocloseable danger message\n              ")
            .and("have.css", "background-color").and("eq" , "rgb(217, 83, 79)");
        cy.get('#normal-btn-danger').should("be.visible")
            .and("have.text", "\n              Normal danger message \n              ")
            .and("have.css", "background-color").and("eq", "rgb(217, 83, 79)");
        cy.get('#autoclosable-btn-info').should("be.visible")
            .and("have.text", "\n              Autocloseable info message\n              ")
            .and("have.css", "background-color").and("eq", "rgb(91, 192, 222)");
        cy.get('#normal-btn-info').should("be.visible")
            .and("have.text", "\n              Normal info message\n              ")
            .and("have.css", "background-color").and("eq", "rgb(91, 192, 222)");
    })

    it('The user click on the "autoclosable" button.', () =>{
        cy.get('#autoclosable-btn-success').click();
        cy.get('.alert-autocloseable-success').should("be.visible")
            .and("have.text", "\n              I\'m an autocloseable success  message. I will hide in 5 seconds.\n            ")
            .and("have.css", "background-color").and("eq", "rgb(223, 240, 216)")
        cy.wait(5000);
        cy.get('.alert-autocloseable-success').should("not.be.visible");
    });

    it('The user click on the "Normal" button.', () =>{
        cy.get('#normal-btn-success').click();
        cy.get('.alert-normal-success').should("be.visible")
            .and("have.text", "\n              ×\n              I\'m a normal success message. To close use  the appropriate button.\n            ")
            .and("have.css", "background-color").and("eq", "rgb(223, 240, 216)")
        cy.get('.alert-normal-success > .close').should("be.visible");
        cy.get('.alert-normal-success > .close').click();
        cy.get('.alert-normal-success > .close').should("not.be.visible");
    });

    it('The user click on the "Autocloseable" button.', () =>{
        cy.get('#autoclosable-btn-warning').click()
                .should("have.text", "\n              Autocloseable warning message\n              ")
                .and("have.css", "background-color").and("eq", "rgb(240, 173, 78)");
        cy.get('.alert-autocloseable-warning').should("be.visible")
            .and("have.text", "\n              I\'m an autocloseable warning message. I will hide in 3 seconds.\n            ")
            .and("have.css", "background-color").and("eq", "rgb(252, 248, 227)");
        cy.wait(3000);
        cy.get('.alert-autocloseable-warning').should("not.be.visible");
    });

    it('The user click on the "Normal warning button', () => {
        cy.get('#normal-btn-warning').click();
        cy.get('.alert-normal-warning').should("be.visible")
            .and("have.text", "\n              ×\n              I\'m a normal warning message. To close use  the appropriate button.\n            ")
            .and("have.css", "background-color").and("eq", "rgb(252, 248, 227)");
        cy.get('.alert-normal-warning > .close').click();
        cy.get('.alert-normal-warning').should("not.be.visible");
    })

    it('The user click on the "Autocloseable danger" button', () => {
        cy.get("#autoclosable-btn-danger").click();
        cy.get('.alert-autocloseable-danger').should("be.visible")
            .and("have.text", "\n              I\'m an autocloseable danger message. I will hide in 5 seconds.\n            ")
            .and("have.css", "background-color").and("eq", 'rgb(242, 222, 222)');
        cy.wait(5000);
        cy.get('.alert-autocloseable-danger').should("be.not.visible");
    })

    it('The user click on the "Normal danger button" button', () => {
        cy.get('#normal-btn-danger').click();
        cy.get('.alert-normal-danger').should("be.visible");
        cy.get('.alert-normal-danger')
            .should("have.text", "\n              ×\n              I\'m a normal danger message. To close use  the appropriate button.\n            ")
            .and("have.css", "background-color")
            .and("eq", "rgb(242, 222, 222)");
        cy.get('.alert-normal-danger > .close').click();
        cy.get('.alert-normal-danger').should("not.be.visible");
    });

    it('The user click on the "Autocloseable info" button', () => {
        cy.get('#autoclosable-btn-info').click();
        cy.get('.alert-autocloseable-info').should("be.visible")
            .and("have.text", "\n              I\'m an autocloseable info message. I will hide in 6 seconds.\n            ")
            .and("have.css", "background-color").and("eq", 'rgb(217, 237, 247)');
        cy.wait(6000);
        cy.get('.alert-autocloseable-info').should("be.not.visible");
    });

    it('The user click on the "Normal info message" button',() => {
        cy.get('#normal-btn-info').click();
        cy.get('.alert-normal-info').should("be.visible")
            .and("have.text", "\n              ×\n              I\'m a normal info message. To close use  the appropriate button.\n            ")
            .and("have.css", "background-color").and("eq", "rgb(217, 237, 247)");
        cy.get('.alert-normal-info > .close').click();
        cy.get('.alert-normal-info').should("not.be.visible");
    });
});
