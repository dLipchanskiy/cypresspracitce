describe('Bootstrap page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/bootstrap-download-progress-demo.html');
    });

    it('Check all elements.', () => {
        cy.get('.row > .panel > .panel-body').should("be.visible");
        cy.get('.prog-circle').should("be.visible");
        cy.get('#cricle-btn').should("be.visible")
            .and("be.enabled").and("have.text", " Download")
    });

    it('The user click on the "Download" button', () => {
        cy.get('.fill').should("have.css", "background-color",);
        cy.get('#cricle-btn').click();
        cy.get('.percenttext').should("be.visible")
    })
});
