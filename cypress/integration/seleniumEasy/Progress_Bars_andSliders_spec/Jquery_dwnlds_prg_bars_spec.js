describe('JQueryProgressBar page', () => {
    beforeEach(() => {
        cy.visit('https://www.seleniumeasy.com/test/jquery-download-progress-bar-demo.html')
    });

    it('Check elements on the page.', () => {
        cy.get('.row').should("be.visible");
        cy.get('#downloadButton').should("be.visible")
            .and("be.enabled").and("have.text", 'Start Download');
    });

    it('The user click on the "Start Download button', () => {
        cy.get('#downloadButton').click();
        cy.get('.progress-label').should("be.visible");
        cy.get('#dialog').should("be.visible");
    })
});
