describe(' The user navigate to the jquery page', () => {
    beforeEach( () => {
        cy.visit('https://www.seleniumeasy.com/test/jquery-date-picker-demo.html')
    });

    it('The user should navigate to the page', () => {
        cy.get('.col-md-6 > .panel').should("be.visible");
        cy.get('#from').should("be.empty");
        cy.get('#to').should("be.empty");
    });

    it('The user should be select START and END date', () => {
        cy.get('#from').click();
        cy.get(':nth-child(3) > :nth-child(3) > .ui-state-default').click();
        cy.get('#to').click();
        cy.get(':nth-child(4) > :nth-child(4) > .ui-state-default').click();
        cy.get('#from').should("have.value", "12/17/2019");
        cy.get('#to').should("have.value", "12/25/2019");
    });
});
