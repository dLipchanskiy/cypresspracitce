describe('bootstrap date pickers', () => {
    beforeEach( () => {
        cy.visit('https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html')
    });

    it('The user should be navigate to the page.', () => {
       cy.url().should("include", "/test/bootstrap-date-picker-demo.html");
        cy.get(':nth-child(2) > .panel').should("be.visible");
        cy.get(':nth-child(3) > .panel').should("be.visible");
        cy.get('#sandbox-container1 > .input-group > .form-control')
            .type('28122019{enter}');
        cy.get('#sandbox-container1 > .input-group > .form-control')
            .should("have.value", "16/12/2019")
    });

    it('The user should be select date via datepicker', () => {
        cy.get('.input-group-addon > .glyphicon').click();
        cy.get('.datepicker-days > .table-condensed > tfoot > :nth-child(1) > .today')
            .click();
        cy.get('#sandbox-container1 > .input-group > .form-control')
            .should("have.value", "16/12/2019")
    });

    it('The user should be clear the date' , () => {
        cy.get('.input-group-addon > .glyphicon')
            .click();
        cy.get('.datepicker-days > .table-condensed > tfoot > :nth-child(1) > .today')
            .click();
        cy.get('#sandbox-container1 > .input-group > .form-control')
            .should("have.value", "16/12/2019");
        cy.get('.input-group-addon > .glyphicon')
            .click();
        cy.get('.datepicker-days > .table-condensed > tfoot > :nth-child(2) > .clear').click();
        cy.get('#sandbox-container1 > .input-group > .form-control')
            .should("have.value", '');
    });

    it('The user should be visible the Date range example', () => {
        cy.get(':nth-child(3) > .panel').should("be.visible");
        cy.get('[placeholder="Start date"]').should("be.empty");
        cy.get('[placeholder="End date"]').should("be.empty");
    });

    it('The user should be select the start date and end date', () => {
        cy.get('[placeholder="Start date"]').click();
        cy.get('tbody > :nth-child(3) > :nth-child(2)').click();
        cy.get('[placeholder="End date"]').click();
        cy.get(':nth-child(4) > :nth-child(4)').click();
        cy.get('[placeholder="Start date"]').should("have.value", "09/12/2019");
        cy.get('[placeholder="End date"]').should("have.value", "18/12/2019");
    });
});
